<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Toko Online | Tentang Kami</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="fontawesome/css/all.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <?php require "navbar.php"; ?>

    <!-- banner -->
    <div class="container-fluid banner2 d-flex align-items-center">
        <div class="container">
            <h1 class="text-white text-center">Tentang Kami</h1>
        </div>
    </div>

    <!-- main -->
    <div class="container-fluid py-5">
        <div class="container fs-5 text-center">
            <p>Toko Online marketplace yang melayani konsumen yang mencari produk dari merek dan 
                distributor resmi lokal maupun internasional. Kami Menggunakan teknologi 
                untuk menghubungkan dan menumbuhkan ekosistem, dari menjangkau populasi yang tidak memmiliki rekening bank, hingga memberikan nilai lebih kepada produsen seperti petani dan nelayan. Toko Online kami bekerjasama dengan 13 partner logistik dan 
                fulfillment yang dilengkapi dengan layanan pengiriman di hari yang sama dengan sistem yang terintegrasi. Penjual juga dapat menyimpan produk di gudang pintar kami yang berlokasi di seluruh Indonesia. </p>
            <p>
            Layanan E-commerce Toko Online menyediakan berbagai pilihan produk melalui Marketplace, Official Stores, Instant Commerce, Interactive Commerce, dan Rural Commerce.
            Tokopedia menyediakan platform teknologi periklanan untuk membantu penjual mempromosikan bisnis mereka, menarik lebih banyak konsumen, dan meningkatkan penjualan lewat layanan Pay for Performance 
            “P4P” Advertising, Display Advertising, dan Customised Marketing Packages.
            Ekosistem khusus untuk mempermudah masyarakat, terutama orang tua, dalam memenuhi kebutuhan serta informasi relevan berkaitan dengan keluarga dan rumah tangga.
            </p>    
            <p>
            Tokopedia di bawah nama Tokopedia Bersama, memiliki komitmen untuk dapat menjembatani dan meningkatkan kepedulian sosial guna memaksimalkan kapasitas potensi dari kelompok masyarakat yang membutuhkan.
                “Beraksi untuk Sesama” bermakna bahwa dalam 
                setiap perjalanan dan langkah inisiatif sosial Tokopedia, kami tidak terlepas dari dukungan dan bantuan dari berbagai pihak yang senantiasa bahu membahu untuk berkolaborasi guna meningkatkan
                 kualitas hidup masyarakat. Mengembangkan kapasitas kelompok perempuan dalam kegiatan ekonomi produktif.
            </p>
        </div>
    </div>

    <!-- footer -->
    <?php require "footer.php"; ?>

    <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="fontawesome/js/all.min.js"></script>
</body>
</html>