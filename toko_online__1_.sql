-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 08, 2023 at 04:21 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_online`
--

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama`) VALUES
(1, 'Baju Pria'),
(2, 'Baju Wanita'),
(3, 'Hoddie'),
(4, 'Jam Tangan'),
(5, 'Tas'),
(6, 'Kacamata');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `harga` double NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `detail` text DEFAULT NULL,
  `ketersediaan_stok` enum('habis','tersedia') DEFAULT 'tersedia'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id`, `kategori_id`, `nama`, `harga`, `foto`, `detail`, `ketersediaan_stok`) VALUES
(2, 1, 'Kaos distro murah sablon digital Zodiak', 23800, 'yPqpssjaRKxuKhwIfr6R.jpg', 'Kondisi: Baru&lt;br&gt;\r\nMin. Pemesanan: 1 Buah&lt;br&gt;\r\nEtalase: kaos distro&lt;br&gt;\r\nKaos distro murah sablon digital Zodiak&lt;br&gt; berkualitas atasan Unisex&lt;br&gt;\r\n\r\nKaos dengan bahan Cotton Premium (Tidak Transparan, Halus dan Nyaman dipakai) &lt;br&gt;Kualitas Ekspor.&lt;br&gt;\r\n\r\nDesign UniSex yang cocok dipakai Pria maupun Wanita.\r\n\r\nWarna warna yang tersedia\r\n1.putih\r\n2.abu abu\r\n3 navy\r\n4.maroon\r\n5.pink baby\r\n6.hitam\r\n7.army\r\n\r\nTersedia dalam ukuran (Lebar 1/2 lingkar x Panjang)\r\n\r\nM : 48 cm x 68 cm\r\nL : 50 cm x 70 cm\r\nXL : 52 cm x 72 cm\r\nXXL : 54 cm x 74 cm\r\n\r\nNOTE : Cantumkan Warna &amp; Ukuran yang diinginkan pada Keterangan saat melakukan pembelian!\r\n', 'tersedia'),
(3, 1, 'KAOS COTTON PRIA DEWASA - Abu Tua(KOPI)', 17990, 'UjoTOJ0vnStYl7pGvyWM.jpg', 'Kondisi: Baru<br>\r\nMin. Pemesanan: 1 Buah<br>\r\nEtalase: Semua Etalase<br>\r\nKAOS PRIA DEWASA MURAH MERIAH\r\nSize: M L XL XXL<br>\r\n\r\nREADY STOCK !!!<br><br>\r\nMURAH MERIAH KARNA KITA MEMPRODUKSI SENDIRI🙏\r\n\r\nBahan Cotton Soft Tebal<br>\r\nCocok buat jalan-jalan dan keseharian di rumah santai tidak panas<br>\r\n\r\n• Bahan kain katun halus\r\nbahan adem/tidak panas,nyerap keringat nyaman\r\ndipakai<br>\r\n• warna tidak luntur<br>\r\n• cocok untuk orang dewasa cowok/cewek<br>\r\n\r\nKETERANGAN UKURAN:<br>\r\n● M : Lebar dada 42cm, panjang 60cm<br>\r\n● L : Lebar dada 48cm, panjang 64cm<br>\r\n● XL: Lebar dada 51cm, panjang 71cm<br>\r\n● XXL: Lebar dada 59cm, panjang 74cm<br>\r\n•XXXL:Lebar dada 62cm, panjang 75cm<br>\r\n\r\nSemua warna READY STOCK !!!<br>\r\n\r\nTersedia dalam 10 warna :<br>\r\n\r\n- Hitam<br>\r\n- Putih<br>\r\n- Marron<br>\r\n- Navy<br>\r\n- Merah<br>\r\n- Abu muda<br>\r\n- Hijau Army<br>\r\n- Benhur<br><br>\r\n- Abu Muda<br>\r\n-Mustard<br>\r\n\r\npengiriman setiap hari<br>\r\nKita proses sesuai urutan pesanan yg masuk🙏<br>\r\n<br>\r\n\r\nSilahkan diorder _ Terimakasih 🙏<br>\r\n#Kaos#baju#pakaianpria#kemeja#distro#kaospria#bajumurah#grosiran#bajukaos#sweater#grosirdistro#kaosmurah#kaosterbaru#bajulenganpendek#kaospendek#fashionpria#bajucowok#oblong#kaosoblong#oblongdewasa</br>', 'tersedia'),
(4, 1, 'kaos distro away sablon digital cotton', 23000, 'h11XmgpQou31Udca2qeK.jpg', 'Kondisi: Baru<br>\r\nMin. Pemesanan: 1 Buah<br>\r\nEtalase: kaos distro<br>\r\nWelcome To Empire Official Store<br>\r\n\r\nKAOS DISTRO SAIL AWAY SABLON DIGITAL<br><br> COWOK CEWEK/UNISEX\r\n\r\nBAHAN : COTTON COMBED 30S<br>\r\n(ADEM, LEMBUT, DAN COCOK BUAT SEHARI HARI)<br>\r\n\r\nAVAILABLE SIZE M,L,XL,XXL<br>\r\n\r\nSize Chart :<br>\r\n\r\n- M : Lebar Dada 48cm x Panjang Badan 68cm<br>\r\n\r\n- L : Lebar Dada 50cm x Panjang Badan 70cm<br>\r\n\r\n- XL : Lebar Dada 52cm x Panjang badan 72cm<br>\r\n\r\n-XXL : Lebar Dada 54cm x Panjang Badan 74cm<br>\r\n\r\nSABLON DIGITAL (RAPIH, AWET, DAN TAHAN LAMA)TIDAK DISABLON LAGI<br>\r\n\r\nMEMBELI=SETUJU✅, NO COMPLAIN ❌<br>\r\n\r\n⛔PERATURAN TOKO⛔\r\nMohon Lakukan Video Unboxing Saat Paket Diterima 🎥<br>\r\nProduk yang masih bisa di klik Variasi = Ready Stok boleh langsung di order ✅\r\nTidak bisa di Klik = Kosong ❎<br>\r\njika ada kesalahan produk / kurang barang, harap Melampirkan video unboxing, foto produk dan Label pengiriman. 🎥📄<br>\r\n\r\nFeedback 💬<br>\r\n👍(1) Kalau kamu puas dengan produk kami, mohon untuk meninggalkan komentar yang positif (serta rating bintang 5). Kamu juga dapat bantu menyebarkan kepada teman-teman di Facebook, Twitter, dll. <br>💖\r\n👍<br>(2) Mohon untuk hubungi kami sebelum meninggalkan komentar yang negatif atau mengajukan dispute diTokopedia💖\r\n👍<br>(3) Mohon untuk memberikan kesempatan bagi kami untuk menyelesaikan masalah, Terima kasih.</br>', 'tersedia'),
(6, 2, 'REISTA SET ROK TERBARU', 384000, 'C71QkcZo06u1XAVFJf0t.jpg', '1 set sudah termasuk semua ini:<br>\r\n- Cardy oversize model crop lengan balon, dengan saku di kiri kanannya dan kancing bisa pakai model ditutup atau dibuka, tambah keren.<br>\r\n- Inner Manset model highneck, uuuh tambah cute.<br>\r\n- Promo free jilbab segi empat.</br>', 'tersedia'),
(7, 2, 'ZANETTA 4IN1 PREMIUM OOTD SET HIJAB LENGKAP HEMAT | SETELAN BAJU', 279450, 'HP5LhHGVj8HFKXcgh2Ml.jpg', '4 in 1 set paket OOTD lengkap premium praktis tinggal pakai, yang terdiri dari:<br>\r\n\r\n✔Adjustable stripes vest, bahan knitted halus lembut dan lentur. Talinya bisa diikatkan ke samping pinggang maupun dibiarkan loose begitu aja, sama cakepnya untuk stylingnya 👌🥰<br>\r\n✔Puff blouse kekinian, bahan mosscrepe, bida menambah kesan keren banget pas dipakai, menciptakan paduan yang keren dan unik saat dipakainya. LD max 105 cm.<br>\r\n✔Kulot high waist dengan cutting lurus, cutting ini dijamin bikin penampilan kamu lebih jenjang, ramping, dan pastinya nyaman karena flowy dan adem, serta kelenturannya akan membebaskanmu saat aktif bergerak. Bagian belakang berkaret, bagian samping kiri kanan terdapat daku aktif, sleting depan dan kancing.<br>\r\n✔PROMO GRATIS PASHMINA Cerutty babydoll HQ ukuran 175 x 75 cm untuk setiap pembelian 1 set ini, membuatmu tidak perlu bingung lagi untuk padu padan koleksi outfit harianmu, tinggal cuss aja 💖💖</br>', 'tersedia'),
(8, 2, 'Atasan Blouse Wanita RAISA BLOUSE', 68750, 'NpB2A6tqXE8n5ztWeU8M.jpg', 'Bahan CREPE Fit XL Lingkar DADA : 108CM&lt;br&gt; Panjang BAJU : 63CM Ready Stok,&lt;br&gt; Siap Kirim Sesuai Foto, Goodquality&lt;/br&gt;', 'tersedia'),
(9, 3, 'Sweater STUDY Hoddie', 60000, '9d0Zon3sxLPbIhoFpeHe.jpg', 'Bahan Fleece Mix Sablon<br>\r\n<br>\r\nSize XL<br>\r\nLd 120. pjg badan 70. pjg tangan 65.</br>', 'tersedia'),
(10, 3, 'Outerwear Sweater Wanita Hoddie ', 57000, '78a0LOn4GUqzrYTr0tkP.jpg', 'Detail Ukuran:<br>\r\n- Lingkar Dada : 104cm<br>\r\n- Panjang Baju : 63cm<br>\r\n- Panjang Tangan : 54cm</br>', 'tersedia'),
(11, 3, 'Sweater sheep hoddie', 57000, 'd66CdVV4zG0tTayJGR5I.jpg', 'ETERANGAN PRODUK\r\n🌸Bahan : Fleece\r\n🌸Ukuran : Allsize Fit L\r\n🌸Ld : 100 – 102 cm\r\n🌸Toleransi Ukuran 1-2 cm\r\n\r\nWARNA YANG TERSEDIA\r\n- Coksu\r\n- Moca\r\n\r\n• Gambar hanya Sebagai referensi\r\n• Tingkat kemiripan 80-90%', 'tersedia'),
(12, 3, 'Sweater wanita/ dino sweater hoddie wanita', 79000, 'kcYmHGa5BNsWMCxg9AOJ.jpg', '✔detail Tulisan : Sablon<br>\r\n✔UKURAN All size<br>\r\n✔LD = -+120 cm PJ = -+65 hanya ada 1 ukuran )<br>\r\n✔Bahan : FLEECE cotton Premium<br>\r\n\r\n✔jahitan rapi dan kuat dan tdk mudah kusut<br>\r\n✔ Garansi uang kembali , jika produk tdk sesuai dengan gambar<br>\r\n✔ Bahan Lembut Tebal Adem dan nyaman</br>', 'tersedia'),
(13, 4, 'REBIRTH Jam Tangan Wanita Tali Kulit Anggun ', 94000, 'W9XBVxlFsUVYpajQZHQI.jpg', 'Nama Merek: REBIRTH<br>\r\nNomor Model: 2037<br>\r\nBIDEN jam tangan asli<br>\r\nJenis proyek:Jam Quartz<br>\r\nFitur: tahan air<br>\r\nPergerakan: kuarsa</br>', 'tersedia'),
(14, 4, 'Jam Tangan Wanita Alexandre Christie AC 2A14', 98000, 'HI46KZ1dEkb61I5Tyvsq.jpg', 'Jam Keren Wanita ALEXANDRE CHRISTIE AC 2A14 Ladies Multifunction Leather Strap<br>\r\n\r\n* Bahan casing: stainless steel<br>\r\n* Strap: leather<br>\r\n* Kaca crystal mineral<br>\r\n* Water resistant 3 ATM (30 meter/bar)<br>\r\n* Garansi service dan spare parts 12 bulan<br>\r\n* Buku garansi dan petunjuk penggunaan<br>\r\n* Quartz movement<br>\r\n* Sudah termasuk wooden box exclusive Alexandre Christie</br>', 'tersedia'),
(15, 5, 'Tas selempang wanita korea serut Eloise Khaki Beauty Gum', 89000, 'BWQ1SR5ATHH6dvTncQWy.png', 'Produk ini memiliki strap panjang, sehingga bisa dijadikan sling bag ataupun shoulder bag.<br>\r\n\r\nterbuat dari bahan PU leather kualitas import dengan bahan tebal, tidak akan <br>nyangkut furing didalamnya apabila dibuka tutup.<br> Harga sangat terjangkau dengan kualitas premium.</br>', 'tersedia'),
(16, 5, 'Berrybenka', 158000, 'zADcwukMnq8sck3o4bNe.jpg', 'Sempurnakan tampilanmu dengan tas ini! <br>Cocok untuk dipadankan dengan berbagai macam look favoritmu!<br>\r\n\r\nUkuran :<br>\r\nTinggi: 13 cm<br>\r\nLebar: 18 cm<br>\r\nAlas: 7 cm<br>\r\nPanjang tali: 85 cm</br>\r\n', 'tersedia'),
(17, 5, 'Pamole', 149000, '5v3cKnVj2QOYMJ3TOKA1.jpg', 'Tas selempang ini cocok banget buat dipakai hangout, acara formal ataupun non formal.<br> Dilengkapi dengan handle di bagian atasnya, sehingga bisa juga untuk jadi hand bag.<br>\r\nTali dibuat dengan webbing katun yang kuat, bisa dilepas dan panjangnya bisa diatur sesuai keinginan. <br>Semua sisi tas dilapisi dengan busa sehingga lebih kuat dan tegak.</br>', 'tersedia'),
(18, 6, 'Kacamata Terbaru Wanita Kotak Style', 10000, 'aub9Div39ZpTZiy1hIIX.jpg', 'ELSA menjual Kacamata Import Fashion, <br>Murah, dan Kualitas Terjamin.<br>\r\nProduk IMPORT bukan LOKAL<br>\r\nTerima DROPSIHP</br>', 'tersedia');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'admin', '$2y$10$FAkmfXgv4Q4zS4qPKeNOIujgoC//2NdZU34b75rKyO410MFU54Tue');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nama` (`nama`),
  ADD KEY `kategori_produk` (`kategori_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `produk`
--
ALTER TABLE `produk`
  ADD CONSTRAINT `kategori_produk` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
